#!/bin/bash
echo "SET FOREIGN_KEY_CHECKS=0; CREATE DATABASE IF NOT EXISTS current; DROP DATABASE IF EXISTS latest; CREATE DATABASE latest; USE latest;" > /tmp/init.sql
cat /tmp/db/latest.sql >> /tmp/init.sql

/usr/bin/mysqld &
sleep 5

mysql < /tmp/init.sql

liquibase --changeLogFile=/tmp/changelog.xml migrate

filename=`date +"%Y-%m-%d_%H-%M-%S"`

liquibase diffChangeLog | sed 's/TIMESTAMP(19)/TIMESTAMP/g' | sed 's/type=\"BIT\"/type=\"TINYINT(1)\"/g' > /tmp/db/changelogs/$(echo $filename).xml